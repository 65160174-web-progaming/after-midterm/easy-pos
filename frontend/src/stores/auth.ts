import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import authService from '@/services/auth'
import { useMessageStore } from './message'
import { useRouter } from 'vue-router'
import { useLoadingStore } from './loading'

export const useAuthStore = defineStore('auth', () => {
  const messageStore = useMessageStore()
  const router = useRouter()
  const loadindStore = useLoadingStore()
  const login = async function (email: string, password: string) {
    try {
      loadindStore.doLoad()
      const res = await authService.login(email, password)
      console.log(res.data)
      messageStore.showMessage('Login Success')
      router.replace('/')
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('accessToken', res.data.access_token)
    } catch (error: any) {
      console.log(error.message)
      messageStore.showMessage(error.message)
    }
    loadindStore.finish()
  }

  const logout = () => {
    localStorage.removeItem('user')
    localStorage.removeItem('accessToken')
    router.replace('/login')
  }

  function getCurrentUser(): User | null {
    const userStr = localStorage.getItem('user')
    if (userStr === null) {
      return null
    }
    return JSON.parse(userStr)
  }

  function getToken(): string | null {
    const tokenStr = localStorage.getItem('access_token')
    if (tokenStr === null) {
      return null
    }
    return JSON.parse(tokenStr)
  }

  return { getCurrentUser, login, getToken, logout }
})

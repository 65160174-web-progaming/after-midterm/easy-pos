import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async signIn(
    email: string,
    pass: string,
  ): Promise<{ user: any; access_token: string }> {
    try {
      const user = await this.usersService.findOneByEmail(email);
      if (!user || user.password !== pass) {
        throw new UnauthorizedException();
      }

      const { password, ...result } = user;
      const payload = { id: user.id, email: user.email };
      return {
        user: result,
        access_token: await this.jwtService.signAsync(payload),
      };
    } catch (error) {
      throw new UnauthorizedException();
    }
  }
}
